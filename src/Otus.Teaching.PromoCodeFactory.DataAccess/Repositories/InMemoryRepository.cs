﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data) => Data = data;

        public Task<IEnumerable<T>> GetAllAsync() => Task.FromResult(Data);

        public Task<T> GetByIdAsync(Guid id) => Task.FromResult(Data.FirstOrDefault(x => x.Id == id));


        public Task<T> CreateAsync(T entity)
        {
            entity.GenerateId();

            Data = Data.Append(entity);
            return Task.FromResult(entity);
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            if (Data.Any(x => x.Id == id))
            {
                Data = Data.Where(x => x.Id != id);
                return Task.FromResult(true);
            }
            return Task.FromResult(false);
        }

        public Task<T> UpdateAsync(Guid id, T entity)
        {
            if (Data.Any(x => x.Id == id))
            {
                var item = Data.Select(x => x.Id == id ? entity : x);
                return Task.FromResult(item.First(x => x.Id == id));
            }
            return null;
        }
    }
}