﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeItem

    {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    }
}
